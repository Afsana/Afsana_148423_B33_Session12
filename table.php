<!DOCTYPE html>
<html>
<head>
    <style>
        table, th, td {
            border: 1px solid black;
        }
    </style>
</head>
<body>
<h1><centre>Book Title - List</centre></h1>
<table>
    <tr>
        <th>Serial</th>
        <th>ID</th>
        <th>Book Title</th>
        <th>Action</th>
    </tr>
    <tr>
        <td>1</td>
        <td>5</td>
        <td>PHP-BOOK Title #1</td>
        <td><button>View</button><button>Edit</button><Button>Delete</Button><button>Trash</button></td>
    </tr>
    <tr>
        <td>2</td>
        <td>6</td>
        <td>PHP-BOOK Title #2</td>
        <td><button>View</button><button>Edit</button><Button>Delete</Button><button>Trash</button></td>
    </tr>
    <tr>
        <td>3</td>
        <td>7</td>
        <td>PHP-BOOK Title #3</td>
        <td><button>View</button><button>Edit</button><Button>Delete</Button><button>Trash</button></td>
    </tr>
<tr>
    <td>PAGE : <1,2,3,4,5,6,7></td>
</tr>
    <tr>
        <td><button>Add New Book Title</button><button>View Trash Items</button><button>Download as PDF</button><button>Download as EXCEL File</button></td>
</table>
</body>
</html>