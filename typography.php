<!DOCTYPE html>
<html>
<head>

<h1>This is html tag h1</h1>
<h2>This is html tag h2</h2>
<h3>This is html tag h3</h3>
<h4>This is html tag h4</h4>
<h5>This is html tag h5</h5>
<h6>This is html tag h6</h6>

<p>This text is large but <small> This text is small</small></p>

<p>This is an another paragraph.</p>

<mark>webDesign</mark><br>

<del>this is deleted text.</del><br>

<u>this is underlined text.</u><br>

<b>this is bold text.</b><br>

<i>this is italic text.</i><br>

<blockquote>This is a long quotation.This is a long quotation.</blockquote><br>

The<abbr title="World Health Organization">WHO</abbr> was founded in 1948.<br>

Do the work<acronym title="As Soon As Possible">
    ASAP</acronym>.<br>

<address>
    Written by pondit.com<br>
    <a href="mailto:team@pondit.com">Email us</a><br>
    Address: House#10,Road#4,Dhanmondi<br>
    Phone: 01715******
</address><br>

<bdo dir="rtl">
This is WWW.COM
</bdo><br>

<big>this is big text</big> but this is not.<br>

<cite>This Is a Citation</cite><br>

<p>Continue text. <code>this is code.</code> Continue text.</p><br>

<dfn>Definition item</dfn>
<p><dfn id="def-internet">The Internet</dfn> is a global system of interconnected networks that use the Internet Protocol Suite (TCP/IP) to serve billions of users worldwide.</p><br>

<h1>Inserted text</h1>
<p>I am <del>extremely</del><ins>very</ins> grateful to you if you come to my home.</p><br>

<p>Chemical structure of water is H<sub>2</sub>O.</p><br>

<p>3<sup>3</sup>=27</p><br>

</head>
</html>