<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ChessBoard</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/�/3.3�/css/bootstrap.min.css">
    <style>
        table {
            border: 5px solid #1a0000;
        }
        td {
            height: 50px;
            width: 50px;
        }
        .color_box {
            background: #3C1500;
        }
    </style>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/�/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <h2>Chess Board</h2>
    <table>
        <?php
        for($row = 1; $row <= 8; $row++){
            echo "<tr>";
            for($column = 1; $column <= 8; $column++){
                if(($row + $column) % 2 == 0){
                    echo "<td class='color_box'></td>";
                } else {
                    echo "<td></td>";
                }
            }
            echo "</tr>";
        }
        ?>
    </table>
</div>
</body>
</html>