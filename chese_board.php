<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<style>
    .main {
        width: 200px;
        margin: 0;
        padding: 0;
    }
    .black,.red{
        width: 50px;
        width: 25px;
        height: 25px;
        display: inline-block;
        margin: 0;
        padding: 0;
        background-color: #111;
        vertical-align: bottom;
    }
    .red {
        background-color: olivedrab;
    }
</style>
<body>
<div class="main">
    <?php
    for($i = 1; $i <= 8 ; $i++){
        for($a = 1; $a <= 8; $a++){
            if($i%2 == 0) {
                if($a%2 == 0){
                    echo "<div class='black' > </div>";
                }else{
                    echo "<div class='red' > </div>";
                }
            }else{
                if($a%2 == 0){
                    echo "<div class='red' > </div>";
                }else{
                    echo "<div class='black' > </div>";
                }
            }
        }
    }
    ?>
</div>
</body>
</html>