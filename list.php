<!DOCTYPE html>
<html>
<ul>
    <li>USA</li>
    <li>London</li>
    <li>Japan</li>
</ul>
<br>
<ul style = "list-style-type:circle">
    <li>USA</li>
    <li>London</li>
    <li>Japan</li>
</ul>
<br>
<ul style = "list-style-type:squre">
    <li>USA</li>
    <li>London</li>
    <li>Japan</li>
</ul>
<br>
<ul style = "list-style-type:none">
    <li>USA</li>
    <li>London</li>
    <li>Japan</li>
</ul>
<br>
<ol>
    <li>Dhaka</li>
    <li>Chittagong</li>
    <li>Sylhet</li>
</ol>
<br>
<ol type = "A">
    <li>Dhaka</li>
    <li>Chittagong</li>
    <li>Sylhet</li>
</ol>
<br>
<ol type = "a">
    <li>Dhaka</li>
    <li>Chittagong</li>
    <li>Sylhet</li>
</ol>
<br>
<ol type = "I">
    <li>Dhaka</li>
    <li>Chittagong</li>
    <li>Sylhet</li>
</ol>
<br>
<ol type = "i">
    <li>Dhaka</li>
    <li>Chittagong</li>
    <li>Sylhet</li>
</ol>
<br>
<dl>
    <dt>Sakib Al Hasan</dt>
    <dd>All-Rounder</dd>
    <dt>Musfiqur Rahim</dt>
    <dd>Wicked keeper</dd>
    <dt>Masrafi Bin Murtoza</dt>
    <dd>Bollower</dd>
</dl>
<br>
</html>